package sample;

public class Record {

	private Long id;
	private String data;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Record(Long id, String data) {
		super();
		this.id = id;
		this.data = data;
	}

	public Record() {
		super();
	}

	@Override
	public String toString() {
		return "Record [id=" + id + ", data=" + data + "]";
	}

}