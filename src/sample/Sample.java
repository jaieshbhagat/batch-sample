package sample;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Sample {

	public static final int leftLimit = 97; // letter 'a'
	public static final int rightLimit = 122; // letter 'z'
	public static final int targetStringLength = 10;
	public static final Random random = new Random();

	public static final int BATCH_SIZE = 23;

	public static void main(String[] args) {
		List<Record> records = generateRandomRecords(198);
		int noOfBatchReqd = (int) Math.ceil((double) records.size() / BATCH_SIZE);

		for (int i = 0; i < noOfBatchReqd; i++) {
			System.out.println("Batch " + (i + 1));
			System.out.println(records.size() + BATCH_SIZE);
			int k = i * BATCH_SIZE;
			int l = ((k + BATCH_SIZE) > records.size()) ? records.size() : k + BATCH_SIZE;
			System.out.println(String.format("Start From : %s, End At : %s", k, l));
			for (int j = k; j < l; j++) {
				System.out.println(records.get(j).toString());
			}
		}
	}

	public static List<Record> generateRandomRecords(Integer noOfRecords) {
		List<Record> result = new ArrayList<Record>(noOfRecords);
		for (int i = 0; i < noOfRecords; i++)
			result.add(new Record(Long.valueOf(i + 1), random.ints(leftLimit, rightLimit + 1).limit(targetStringLength)
					.collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString()));
		return result;
	}

}
